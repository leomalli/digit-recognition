# Digit Recognition


## Summary

This repo contains (will contain ...) a simple, bare-bone, and probably incomplete implementation of a neural network architecture.  
It is written in C++ and I decided against using [Eigen](https://eigen.tuxfamily.org) so that I would have to write a simple implementation of a matrix class.


## CURRENT STATE

The dense network class is somewhat functionnal.
I will add a small working example when life gives me time.

## USAGE

You can simply clone the repo and use make to compile the project from the root directory and execute the resulting binary:

    make && ./project

Note that the Makefile is very minimal at the moment, it only compiles in debug mode.


The `exemples/` folder contains some exemples on the `Matrix` class utilisation, and I will add some on the `Network` class in the futur.
You should be able to compile the exemples by running make in the correct directory:

    cd ./exemples && make

Then just execute the binaries.
