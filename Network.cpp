#include "Network.h"

#include <fstream>

#define NOTDONE() \
    do { \
        fprintf(stderr, "%s: %s\tis not implemented yet\n", __FILE__, __func__); \
        exit(1); \
    } while(1)



Network::Network(const int& n_inputs, const int& n_outputs, const int& n_hidden, const double& learningRate)
: m_LearningRate(learningRate)
{
    m_Layers.reserve( n_hidden + 1);
    for (int i = 0; i < n_hidden; ++i)
        m_Layers.emplace_back(n_inputs, n_inputs);

    m_Layers.emplace_back(n_inputs, n_outputs);
}

Network::Network(const std::vector<int>& layout, const double& learningRate)
: m_LearningRate(learningRate)
{
    m_Layers.reserve( layout.size() - 1);
    for (int i = 1; (size_t)i < layout.size(); ++i)
        m_Layers.emplace_back(layout[i-1], layout[i]);

}

void Network::m_ForwardPass( const Matrix<>& inputs )
{
    m_Layers[0].forward( inputs );

    for (size_t i = 1; i < m_Layers.size(); ++i)
        m_Layers[i].forward( m_Layers[i - 1].getActivation() );

}

Matrix<> Network::predict( const Matrix<>& inputs ) const
{
    Matrix<> tmp = inputs;
    for (size_t i = 0; i < m_Layers.size(); ++i)
        tmp = m_Layers[i].predict(tmp);

    return tmp;
}

Matrix<> Network::getOutput() const
{ return m_Layers.back().getRawOut(); }


void Network::fit(const Matrix<>& inputs, const Matrix<>& labels)
{
    std::cout << "Training in progress:\n";
    float progress = 0.0;
    double loss;
    auto [rows, cols] = inputs.getDim();

    for (int i = 0; i < cols; ++i)
    {
        loss = this->m_BackPropagation( inputs.getCol(i), labels.getCol(i) );



        int barWidth = 25;
        std::cout << "[";
        int pos = barWidth * progress;
        for (int i = 0; i < barWidth; ++i) {
            if (i < pos) std::cout << "=";
            else if (i == pos) std::cout << ">";
            else std::cout << " ";
        }
        std::cout << "] " << int(progress * 100.0) << " % loss: ";
        fprintf(stdout, "%.3e\r", loss);
        std::cout.flush();
        progress += 1.0f / (float)cols; // for demonstration only
    }
    std::cout << std::endl;
}

double Network::evaluate( const Matrix<>& inputs, const Matrix<>& labels) const
{
    double loss = 0;
    auto [rows, cols] = inputs.getDim();
    for (int i = 0; i < cols; ++i)
    {
        Matrix<> out = std::move( predict(inputs.getCol(i)) );
        Matrix<> error = out - labels.getCol(i);
        loss += 0.5 * (error.normsqr());
    }
    return loss;
}
double Network::m_BackPropagation(const Matrix<>& input, const Matrix<>& label)
{
    this->m_ForwardPass(input);
    Matrix<> dE( this->getOutput() - label );

    double loss = m_Loss( dE );

    for (size_t i = m_Layers.size(); i > 0; --i )
        dE = m_Layers[i - 1].computeDelta( dE );

    for (Layer& layer : m_Layers)
        layer.updateParameters(m_LearningRate);

    return loss;
}


void Network::setLearningRate(const double& val)
{ m_LearningRate = val; }


// Set the activation functions for all the hidden layers
void Network::setHiddenActivation(const std::string& ActType)
{
    for ( size_t i = 0; i < m_Layers.size() - 1; ++i)
        m_Layers[i].setActivationType( ActType );
}

double Network::m_Loss( const Matrix<>& error ) const
{
    return error.normsqr() * 0.5;
}