#ifndef __LAYER_H__
#define __LAYER_H__
#include <functional>
#include "Matrix.h"

class Layer
{
public:
    Layer(const int& n_inputs, const int& n_outputs);
    Layer(const Layer& other);

    void forward( const Matrix<>& input );
    Matrix<> predict( const Matrix<>& input ) const;

    Matrix<> getActivation() const;
    Matrix<> getRawOut() const;

    Matrix<> computeDelta( const Matrix<>& propagatedError );
    void updateParameters(const double& learningRate);

    Matrix<> getWeights() const;
    Matrix<> getBias() const;

    void setActivationType(const std::string& actType);

private:
    // Parameters
    Matrix<> m_Weights, m_Bias;

    // Informations
    int m_ActivationType;
    Matrix<> m_Inputs, m_Outputs;
    Matrix<> m_Delta;

    // Activation
    Matrix<> m_Activation( Matrix<> input) const;
    Matrix<> m_dActivation( Matrix<> input) const;


    enum
    { LINEAR = 0, RELU };
};

#endif