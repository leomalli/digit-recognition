#ifndef __MATRIX_H__
#define __MATRIX_H__

#include <iostream>
#include <algorithm>
#include <functional>
#include <random>
#include <tuple>

#define NOTDONE() \
    do { \
        fprintf(stderr, "%s: %s\tis not implemented yet\n", __FILE__, __func__); \
        exit(1); \
    } while(1)


/******************************
****      DECLARATION      ****
******************************/

template <class T = double>
class Matrix
{
public:

    Matrix();
    Matrix(const Matrix<T>& other);
    Matrix(Matrix<T>&& other);
    Matrix(const int& rows, const int& cols);
    Matrix(const T& value, const int& rows, const int& cols);
    Matrix(T* data, const int& rows, const int& cols);
    ~Matrix();

    // Assignement operator
    Matrix<T>& operator=(const Matrix<T>& rhs);

    // Const access + Ref access operator
    T& operator()(const int& row, const int& col);
    T operator()(const int& row, const int& col) const;

    // Direct operator access
    T& operator[](const size_t& idx);
    T operator[](const size_t& idx) const;


    // Const functions
    void print() const;
    void printDim() const;
    [[nodiscard]] int getSize() const;
    [[nodiscard]] std::tuple<int, int> getDim() const;
    [[nodiscard]] Matrix<T> transpose() const;    //Only returns the transpose
    [[nodiscard]] Matrix<T> getDiag() const;    //Returns 1xn vector with diag entries
    [[nodiscard]] bool isVector() const;    // Check if we have 1xn or nx1 vector

    // Sum entries by Row/Col
    [[nodiscard]] Matrix<T> rowSum() const;
    [[nodiscard]] Matrix<T> colSum() const;

    // Total sum of entries
    [[nodiscard]] T sum() const;

    // Returns i-th row/col
    [[nodiscard]] Matrix<T> getRow(const size_t& idx) const;
    [[nodiscard]] Matrix<T> getCol(const size_t& idx) const;

    Matrix<T>& fillWith(const T& value);
    Matrix<T>& fillDiag(const T& value);

    // Element wise operations, returns ref to object
    Matrix<T>& elementwise(std::function<T(T)> func);
    Matrix<T>& elementwise(std::function<T(T, int, int)> func);

    // Dot product with given matrix
    [[nodiscard]] Matrix<T> dot(const Matrix<T>& other) const;

    // Return the 2-norm squared
    [[nodiscard]] double normsqr() const;


    // Basic arithmetic operators overload
    Matrix<T>& operator+=(const Matrix<T>& rhs);
    Matrix<T>& operator-=(const Matrix<T>& rhs);
    Matrix<T>& operator*=(const Matrix<T>& rhs);
    Matrix<T>& operator/=(const Matrix<T>& rhs);


    // Some friend function
    template <class V> friend bool operator==(const Matrix<V>& lhs, const Matrix<V>& rhs);
    template <class V> friend bool operator!=(const Matrix<V>& lhs, const Matrix<V>& rhs);

    // Some static members

    // Returns Identity of specified dim
    static Matrix<T> Identity(const int& dim);
    // Create square matrix with digonal given by 1xn vector
    static Matrix<T> Diag(const Matrix<T>& vec);
    // Create nxp from a vector of size 1xp or pxn if vec is px1
    static Matrix<T> DuplicateVec(const Matrix<T>& vec, const int& n);

    // Generate random matrices of given dimensions
    static Matrix<T> Random(const int& rows, const int& cols);
    static Matrix<T> Random(const int& dim);
    // Std normal distribution
    static Matrix<T> RandomNorm(const int& rows, const int& cols);
    static Matrix<T> RandomNorm(const int& dim);


private:
    int m_Rows, m_Cols;
    int m_Size;
    T   *mp_Data;

    // Helper functions
    void allocSpace();

    static double m_RandomUniformDouble();
    static double m_RandomNormalDouble();
    [[nodiscard]] int m_MaxDim() const;

};




/******************************
****    IMPLEMENTATION     ****
******************************/

// Constructors

template <class T>
Matrix<T>::Matrix()
: m_Rows(1), m_Cols(1), m_Size(1), mp_Data(nullptr)
{ allocSpace(); }


template <class T>
Matrix<T>::Matrix(const Matrix<T>& other)
: m_Rows(other.m_Rows), m_Cols(other.m_Cols), m_Size(m_Cols*m_Rows), mp_Data(nullptr)
{
    allocSpace();
    std::copy( other.mp_Data, other.mp_Data + other.m_Size, mp_Data );
}


template <class T>
Matrix<T>::Matrix(Matrix<T>&& other)
{
    m_Rows = std::exchange(other.m_Rows, 0);
    m_Cols = std::exchange(other.m_Cols, 0);
    m_Size = std::exchange(other.m_Size, 0);
    mp_Data = std::exchange(other.mp_Data, nullptr);
}


template <class T>
Matrix<T>::Matrix(const int& rows, const int& cols)
: m_Rows(rows), m_Cols(cols), m_Size(m_Cols*m_Rows), mp_Data(nullptr)
{ allocSpace(); }


template <class T>
Matrix<T>::Matrix(const T& value, const int& rows, const int& cols)
: m_Rows(rows), m_Cols(cols), m_Size(m_Cols*m_Rows), mp_Data(nullptr)
{
    allocSpace();
    fillWith(value);
}


template <class T>
Matrix<T>::Matrix(T* data, const int& rows, const int& cols)
: m_Rows(rows), m_Cols(cols), m_Size(m_Cols*m_Rows), mp_Data(nullptr)
{
    allocSpace();
    for (int i = 0; i < m_Size; ++i)
        mp_Data[i] = data[i];
}


template <class T>
Matrix<T>::~Matrix()
{ delete[] mp_Data; }


template <class T>
Matrix<T>& Matrix<T>::elementwise(std::function<T(T)> func)
{
    std::transform(mp_Data, mp_Data + m_Size, mp_Data, func);
    return *this;
}


// Overload so that if we need the positions of the element we can have them
template <class T>
Matrix<T>& Matrix<T>::elementwise(std::function<T(T, int, int)> func)
{
    for (int i = 0; i < m_Size; ++i)
    {
        int y = i % m_Cols;
        int x = (i - y) / m_Cols;
        mp_Data[i] = func(mp_Data[i], x, y);
    }

    return *this;
}


template <class T>
Matrix<T> Matrix<T>::dot(const Matrix<T>& other) const
{
    if (m_Cols != other.m_Rows )
        throw std::domain_error("Matrix dimension are not matching in matrix multiplication.");

    Matrix<T> result(m_Rows, other.m_Cols);

    for (int i = 0; i < result.m_Rows; ++i)
    {
        for (int j = 0; j < result.m_Cols; ++j)
        {
            T sum = 0;
            for (int k = 0; k < m_Cols; ++k)
                sum += mp_Data[i * m_Cols + k] * other(k, j);

            result(i, j) = sum;
        }
    }
    return result;
}

template <class T>
double Matrix<T>::normsqr() const
{
    if (! this->isVector() )
    {
        std::cerr << "Cannot compute 2-norm of non-vector yet.\n";
        NOTDONE();
    }

    return std::transform_reduce(mp_Data, mp_Data + m_Size, 0.0,
        [](const T& a, const T& b) -> T { return (a+b); },
        [](const T& d)->double{ return static_cast<double>(d*d); });
}

// Returns Identity matrix
template <class T>
Matrix<T> Matrix<T>::Identity(const int& dim)
{
    Matrix<T> result(static_cast<T>(0), dim, dim);
    result.fillDiag(static_cast<T>(1));
    return result;
}

template <class T>
Matrix<T> Matrix<T>::Diag(const Matrix<T>& vec)
{
    // Check if we have one dimensionnal vector
    if (vec.m_Cols != 1 && vec.m_Rows != 1)
        throw std::domain_error("Cannot create diagonal matrix from something other than a vector.");

    int dim = std::max(vec.m_Cols, vec.m_Rows);
    Matrix<T> result(static_cast<T>(0), dim, dim);
    
    // Iterate vector data directly to our diagonal
    for (int i = 0; i < dim; ++i)
        result(i, i) = vec.mp_Data[i];

    return result;
}


template <class T>
Matrix<T> Matrix<T>::DuplicateVec( const Matrix<T>& vec, const int& n)
{
    if (!vec.isVector())
        throw std::domain_error("Cannot convert to Matrix a Non-vector object.");

    bool needTranspose = (vec.m_Cols == 1 );
    Matrix<T> tmp(n, vec.m_MaxDim());

    for (int i = 0; i < n; ++i)
    {
        std::copy(  vec.mp_Data, vec.mp_Data + vec.m_Size,
            tmp.mp_Data + i*vec.m_Size );
    }

    if (needTranspose && (vec.m_MaxDim() > 1)) return tmp.transpose();

    return tmp;

}


template <class T>
Matrix<T> Matrix<T>::Random(const int& rows, const int& cols)
{
    Matrix<T> result(rows, cols);

    for (int i = 0; i < result.m_Size; ++i)
        result.mp_Data[i] = static_cast<T>(m_RandomUniformDouble());

    return result;
}

template <class T>
Matrix<T> Matrix<T>::Random(const int& dim)
{
    return Matrix<T>::Random(dim, dim);
}


template <class T>
Matrix<T> Matrix<T>::RandomNorm(const int& rows, const int& cols)
{
    Matrix<T> result(rows, cols);

    for (int i = 0; i < result.m_Size; ++i)
        result.mp_Data[i] = static_cast<T>(m_RandomNormalDouble());

    return result;
}

template <class T>
Matrix<T> Matrix<T>::RandomNorm(const int& dim)
{
    return Matrix<T>::RandomNorm(dim, dim);
}


template <class T>
T& Matrix<T>::operator()(const int& row, const int& col)
{
    if (row >= m_Rows || col >= m_Cols)
        throw std::domain_error("Trying to access value out of scope");

    return mp_Data[row * m_Cols + col];
}


// Const version
template <class T>
T Matrix<T>::operator()(const int& row, const int& col) const
{
    if (row >= m_Rows || col >= m_Cols)
        throw std::domain_error("Trying to access value out of scope");

    return mp_Data[row * m_Cols + col];
}

template <class T>
T& Matrix<T>::operator[](const size_t& idx)
{ return mp_Data[idx]; }

template <class T>
T Matrix<T>::operator[](const size_t& idx) const
{ return mp_Data[idx]; }


// Fill matrix with given value
template<class T>
Matrix<T>& Matrix<T>::fillWith(const T& value)
{
    for (int i = 0; i < m_Size; ++i)
        mp_Data[i] = value;

    return *this;
}


// Fill the diagonal with given value
template <class T>
Matrix<T>& Matrix<T>::fillDiag(const T& value)
{
    int minDim = std::min(m_Cols, m_Rows);
    for (int d = 0; d < minDim; ++d)
        mp_Data[d * m_Cols + d] = value;

    return *this;
}


template <class T>
void Matrix<T>::print() const
{
    for (int i = 0; i < m_Rows; ++i)
    {
        std::cout << '[';
        for (int j = 0; j < m_Cols - 1; ++j)
        {
            std::cout << mp_Data[i * m_Cols + j] << ',';
        }
        std::cout << mp_Data[i * m_Cols + m_Cols - 1] << "]\n";
    }
}

template <class T>
void Matrix<T>::printDim() const
{ std::cout << '(' << m_Rows << 'x' << m_Cols << ")\n"; }

// Returns the number of element that the matrix contains
template <class T>
int Matrix<T>::getSize() const
{ return m_Size; }


template <class T>
std::tuple<int, int> Matrix<T>::getDim() const
{ return std::tuple<int,int>(m_Rows, m_Cols); }


// Return transpose of current matrix
template <class T>
Matrix<T> Matrix<T>::transpose() const
{
    Matrix<T> dst(m_Cols, m_Rows);

    for (int i = 0; i < m_Rows; ++i)
    {
        for (int j = 0; j < m_Cols; ++j)
        {
            dst(j, i) = mp_Data[i * m_Cols + j];
        }
    }
    return dst;
}


// Return a new 1xn vector containing the diagonal entries
template <class T>
Matrix<T> Matrix<T>::getDiag() const
{
    int minDim = (m_Cols < m_Rows) ? m_Cols : m_Rows;
    Matrix<T> diag(minDim, 1);

    for (int i = 0; i < minDim; ++i)
        diag(i,0) = mp_Data[i * m_Cols + i];

    return diag;
}


template <class T>
bool Matrix<T>::isVector() const
{ return (m_Cols == 1 || m_Rows == 1); }


// Takes mxn -> 1xn
template <class T>
Matrix<T> Matrix<T>::colSum() const
{
    Matrix<T> tmp( std::move(this->transpose().rowSum() ) );
    return tmp.transpose();
}

template <class T>
Matrix<T> Matrix<T>::getRow(const size_t& idx) const
{
    Matrix<T> row(T(), 1, m_Cols);
    for (int i = 0; i < m_Cols; ++i)
        row(0, i) = mp_Data[idx * m_Cols + i];

    return row;
}
template <class T>
Matrix<T> Matrix<T>::getCol(const size_t& idx) const
{
    Matrix<T> col(T(), m_Rows, 1);
    for (int i = 0; i < m_Rows; ++i)
        col(i, 0) = mp_Data[i * m_Cols + idx];

    return col;
}

template <class T>
T Matrix<T>::sum() const
{ return std::reduce(mp_Data, mp_Data + m_Size, static_cast<T>(0) ); }

// Takes mxn -> mx1
template <class T>
Matrix<T> Matrix<T>::rowSum() const
{
    Matrix<T> result(m_Rows, 1);
    for (int i = 0; i < m_Rows; ++i)
    {
        result.mp_Data[i] = std::reduce(
            mp_Data + i*m_Cols, mp_Data + (i+1)*m_Cols, static_cast<T>(0));
    }
    return result;
}


// Some overloads
template <class T>
Matrix<T>& Matrix<T>::operator+=(const Matrix<T>& rhs)
{
    if (m_Cols != rhs.m_Cols || m_Rows != rhs.m_Rows)
        throw std::domain_error("Dimension are not matching for matrix addition");

    for (int i = 0; i < m_Size; ++i)
        mp_Data[i] += rhs.mp_Data[i];

    return *this;
}


template <class T>
Matrix<T>& Matrix<T>::operator-=(const Matrix<T>& rhs)
{
    if (m_Cols != rhs.m_Cols || m_Rows != rhs.m_Rows)
        throw std::domain_error("Dimension are not matching for matrix substraction");

    for (int i = 0; i < m_Size; ++i)
        mp_Data[i] -= rhs.mp_Data[i];

    return *this;
}


// This is elementwise, use dot() for matrix multplication
template <class T>
Matrix<T>& Matrix<T>::operator*=(const Matrix<T>& rhs)
{
    if (m_Cols != rhs.m_Cols || m_Rows != rhs.m_Rows)
        throw std::domain_error("Dimension are not matching in elementwise matrix multiplication");

    for (int i = 0; i < m_Size; ++i)
        mp_Data[i] *= rhs.mp_Data[i];

    return *this;
}


// This is elementwise
template <class T>
Matrix<T>& Matrix<T>::operator/=(const Matrix<T>& rhs)
{
    if (m_Cols != rhs.m_Cols || m_Rows != rhs.m_Rows)
        throw std::domain_error("Dimension are not matching in elementwise matrix division");

    for (int i = 0; i < m_Size; ++i)
        mp_Data[i] /= rhs.mp_Data[i];

    return *this;
}


// Assignement operator
template <class T>
Matrix<T>& Matrix<T>::operator=(const Matrix<T>& rhs)
{
    // If not the same dimension we have to reallocate space
    if ( m_Cols != rhs.m_Cols || m_Rows != rhs.m_Rows )
    {
        m_Cols = rhs.m_Cols;
        m_Rows = rhs.m_Rows;
        m_Size = rhs.m_Size;
        allocSpace();
    }

    // Copy the memory
    std::copy(rhs.mp_Data, rhs.mp_Data + rhs.m_Size, mp_Data);

    return *this;
}


/******************************
****   HELPER FUNCTIONS    ****
******************************/

// Allocate space to the matrix
template <class T>
void Matrix<T>::allocSpace()
{
    if (mp_Data != nullptr) delete[] mp_Data;
    mp_Data = new T[m_Size];
}

template <class T>
double Matrix<T>::m_RandomNormalDouble()
{
    static std::random_device rd;
    static std::mt19937 tw(rd());
    static std::normal_distribution<double> dist(0.0, 1.0);

    return dist( tw );
}

template <class T>
double Matrix<T>::m_RandomUniformDouble()
{
    static std::random_device rd;
    static std::mt19937 tw(rd());
    static std::uniform_real_distribution<double> dist(0.0, 1.0);

    return dist( tw );
}

template <class T>
int Matrix<T>::m_MaxDim() const
{ return std::max(m_Rows, m_Cols); }


/******************************
****   OUTSIDE OVERLOAD    ****
******************************/

template <class T>
std::ostream& operator<<(std::ostream& os, const Matrix<T>& mat)
{
    auto [rows, cols] = mat.getDim();
    for (int i = 0; i < rows; ++i)
    {
        os << '[';
        for (int j = 0; j < cols - 1; ++j)
        {
            os << mat[i * cols + j] << ',';
        }
        os << mat[i * cols + cols - 1] << ']';
        if (i < rows - 1) os << '\n';
    }
    return os;
}


template <class T>
Matrix<T> operator+(const Matrix<T>& lhs, const Matrix<T>& rhs)
{
    Matrix<T> tmp = lhs;
    tmp += rhs;
    return tmp;
}


template <class T>
Matrix<T> operator-(const Matrix<T>& lhs, const Matrix<T>& rhs)
{
    Matrix<T> tmp = lhs;
    tmp -= rhs;
    return tmp;
}


// Note that this is elementwise multiplication
template <class T>
Matrix<T> operator*(const Matrix<T>& lhs, const Matrix<T>& rhs)
{
    Matrix<T> tmp = lhs;
    tmp *= rhs;
    return tmp;
}


// Note that this is elementwise division
template <class T>
Matrix<T> operator/(const Matrix<T>& lhs, const Matrix<T>& rhs)
{
    Matrix<T> tmp = lhs;
    tmp /= rhs;
    return tmp;
}


// Scalar version of the overloads
template <class T>
Matrix<T> operator+(const Matrix<T>& mat, const T& scalar)
{
    Matrix<T> tmp = mat;
    tmp.elementwise([scalar](const T& x) -> T {
        return x + scalar;
    });
    return tmp;
}


template <class T>
Matrix<T> operator-(const Matrix<T>& mat, const T& scalar)
{
    Matrix<T> tmp = mat;
    tmp.elementwise([scalar](const T& x) -> T {
        return x - scalar;
    });
    return tmp;
}


template <class T>
Matrix<T> operator*(const Matrix<T>& mat, const T& scalar)
{
    Matrix<T> tmp = mat;
    tmp.elementwise([scalar](const T& x) -> T {
        return x * scalar;
    });
    return tmp;
}


// Note that the division doesn't commute hence we don't overload it in the definitions under
template <class T>
Matrix<T> operator/(const Matrix<T>& mat, const T& scalar)
{
    Matrix<T> tmp = mat;
    tmp.elementwise([scalar](const T& x) -> T {
        return x / scalar;
    });
    return tmp;
}


// Little dirty work around so that we can commute the operation above...
template <class T>
Matrix<T> operator+(const T& scalar, const Matrix<T>& mat)
{ return (mat + scalar); }
template <class T>
Matrix<T> operator-(const T& scalar, const Matrix<T>& mat)
{ return (mat - scalar); }
template <class T>
Matrix<T> operator*(const T& scalar, const Matrix<T>& mat)
{ return (mat * scalar); }


// Some boolean overloads

template <class T>
bool operator==(const Matrix<T>& lhs, const Matrix<T>& rhs)
{
    if (lhs.m_Cols != rhs.m_Cols || lhs.m_Rows != rhs.m_Rows) return false;

    for (int i = 0; i < lhs.getSize(); ++i)
        if (lhs.mp_Data[i] != rhs.mp_Data[i]) return false;

    return true;
}


template <class T>
bool operator!=(const Matrix<T>& lhs, const Matrix<T>& rhs)
{ return !(lhs == rhs); }

#endif