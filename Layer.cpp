#include "Layer.h"
#include <functional>
#include <fstream>

//#define DEBUG



#define NOTDONE() \
    do { \
        fprintf(stderr, "%s: %s\tis not implemented yet\n", __FILE__, __func__); \
        exit(1); \
    } while(1)

#define SQRT2 1.4142135623730951


Layer::Layer(const int& n_inputs, const int& n_outputs)
:   m_Weights( Matrix<>::RandomNorm(n_inputs, n_outputs) * ( SQRT2 / sqrt((double)n_inputs) ) ),
    m_Bias( Matrix<>(0.0, n_outputs, 1)),
    m_ActivationType(LINEAR),
    m_Inputs(Matrix<>()), m_Outputs(Matrix<>()),
    m_Delta( Matrix<>() )
{ }

Layer::Layer(const Layer& other)
:   m_Weights( other.m_Weights ),
    m_Bias( other.m_Bias ),
    m_ActivationType( other.m_ActivationType ),
    m_Inputs( other.m_Inputs ), m_Outputs( other.m_Outputs ),
    m_Delta( other.m_Delta )
{ }

void Layer::forward( const Matrix<>& input )
{
    m_Inputs = input;
    m_Outputs = m_Weights.transpose().dot( m_Inputs ) + m_Bias;
}

Matrix<> Layer::predict( const Matrix<>& input ) const
{
    return m_Activation( (m_Weights.transpose().dot( input ) + m_Bias) );
}


Matrix<> Layer::getActivation() const
{
    Matrix<> tmp( m_Outputs );
    return m_Activation( tmp );
}

Matrix<> Layer::getRawOut() const
{ return m_Outputs; }


Matrix<> Layer::computeDelta( const Matrix<>& propagatedError )
{
    Matrix<> tmp( m_Outputs );

    //tmp.elementwise( m_dActivation );
    tmp = this->m_dActivation( tmp );

    m_Delta = tmp * propagatedError;
    return m_Weights.dot( m_Delta );
}

void Layer::updateParameters(const double& learningRate)
{
    std::ofstream myFile;
    Matrix<>    grad_Bias = m_Delta,
                grad_Weights = m_Inputs.dot( m_Delta.transpose() );

    m_Bias -= (learningRate) * grad_Bias;
    m_Weights -= (learningRate) * grad_Weights;
}

Matrix<> Layer::getWeights() const
{ return m_Weights; }
Matrix<> Layer::getBias() const
{ return m_Bias; }

Matrix<> Layer::m_Activation( Matrix<> input) const
{
    switch (m_ActivationType)
    {
    case LINEAR:
        return input;
        break;
    case RELU:
        return input.elementwise([](const double& v)->double{return (v>0) * v;});
        break;
    
    default:
        std::cerr << "This activation is not implemented for some reasons.\n";
        exit(EXIT_FAILURE);
        break;
    }

}


Matrix<> Layer::m_dActivation( Matrix<> input) const
{
    switch (m_ActivationType)
    {
    case LINEAR:
        return input.elementwise([](const double& v)->double{return 1.0;});
        break;
    case RELU:
        return input.elementwise([](const double& v)->double{return (double)(v>0);});
        break;
    
    default:
        std::cerr << "This activation is not implemented for some reasons.\n";
        exit(EXIT_FAILURE);
        break;
    }
}

void Layer::setActivationType(const std::string& actType)
{
    if (actType == "linear")
    {
        m_ActivationType = LINEAR;
    }
    else if (actType == "relu")
    {
        m_ActivationType = RELU;
    }
    else
    {
        std::cerr << "The activation: " << actType << " is not yet supported.\n";
        exit(EXIT_FAILURE);
    }
}